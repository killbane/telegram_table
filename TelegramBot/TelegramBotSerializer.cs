﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Google_Docs
{
    class TelegramBotSerializer
    {
        public void SaveUsers(Dictionary<long, User> Users)
        {
            string json = JsonConvert.SerializeObject(Users);
            using (StreamWriter sw = new StreamWriter("users.json"))
            {
                sw.WriteLine(json);
            }
        }

        public Dictionary<long, User> ReadUsers()
        {
            string json;
            if (!System.IO.File.Exists("users.json"))
            {
                return new Dictionary<long, User>();
            }
            using (StreamReader sr = new StreamReader("users.json"))
            {
                json = sr.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<Dictionary<long, User>>(json);
        }
    }
}
