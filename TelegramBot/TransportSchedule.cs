﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Google_Docs
{
    class TransportSchedule
    {
        public string GetTransportSchedule(string url)
        {
            string response;
            using (var client = new WebClient())
            {
                response = HttpUtility.HtmlDecode(client.DownloadString(url));
            }

            return MakeNormal(response);
        }

        private string MakeNormal(string input)
        {
            StringBuilder output = new StringBuilder();
            input = Regex.Replace(input, @"<[^>]*>", "\n");
            input = Regex.Replace(input, @"назад", string.Empty);
            input = Regex.Replace(input, @"\r", string.Empty);
            foreach (var line in input.Split("\n", StringSplitOptions.RemoveEmptyEntries))
            {
                output.AppendLine(line);
            }

            return output.ToString();
        }
    }
}
