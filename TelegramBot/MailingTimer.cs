﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using Telegram.Bot;
using Timer = System.Timers.Timer;
namespace Google_Docs
{
    class MailingTimer
    {
        private Timer timer;
        private static TelegramBotClient _client;
        public static Dictionary<long, User> Users;
        private ITableWorker Parser;
        public MailingTimer(TelegramBotClient client, Dictionary<long, User> users, ITableWorker parser)
        {
            Parser = parser;
            _client = client;
            Users = users;
            timer = new Timer();
            timer.AutoReset = false;
            timer.Elapsed += Tick;
            SetTimer();
        }

        private void SetTimer()
        {
            var timeNow = DateTime.Now;
            timer.Stop();
            var timeToAlarm = new DateTime(timeNow.Year, timeNow.Month, timeNow.Day, 6,30,0);
            if (timeToAlarm < DateTime.Now)
                timeToAlarm = timeToAlarm.AddDays(1);
            timer.Interval = (int)(timeToAlarm - DateTime.Now).TotalMilliseconds;
            timer.Start();
        }

        private void Tick(Object source, ElapsedEventArgs e)
        {
            foreach (var each in Users)
            {
                var flag = false;
                var type = each.Value.GetType();
                foreach (var fields in type.GetFields())
                    if (fields.GetValue(each.Value) == null)
                    {
                        flag = true;
                        break;
                    }
                if(flag)
                    continue;

                var lessons = Parser.GetTodayLessons(each.Value);
                if (lessons[0].ClassRoom == "Дом")
                    break;
                if(!each.Value.NeedMailing)
                    continue;
                var text = $"Расписание на {DateTime.Now.ToShortDateString()}\n{TelegramBot.LessonsToString(lessons)}";
                _client.SendTextMessageAsync(each.Key, text, replyMarkup: TelegramBot.MenuButtons());
                Thread.Sleep(100);
            }
            SetTimer();
        }
    }
}
