﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Google_Docs
{
    class TelegramBot
    {

        private CancellationTokenSource cts;
        private TelegramBotClient client;
        private ITableWorker Parser { get; }
        private Dictionary<long, User> Users { get; set; }
        private IMyLogger Logger;
        private TelegramBotSerializer serializer;
        private MailingTimer timer;
        private enum Buttons
        {
            TodayLessons = 0,
            TomorrowLessons = 1,
            LessonNow = 2,
            LessonNext = 3,
            TransportSG = 4,
            TransportUG = 5
        }
        static string[] buttonNames = { "Расписание на сегодня", "Расписание на завтра", "Текущая пара", "Следующая пара",
            "Транспорт Студенческий городок", "Транспорт Университетский городок" };

        private string url_sg = "http://yartr.ru/rasp.php?vt=1&nmar=22c&q=1&id=302&view=1";
        private string url_ug = "http://yartr.ru/rasp.php?vt=1&nmar=12&q=0&id=865&view=1";

        public TelegramBot(string token, ITableWorker parser, IMyLogger logger)
        {
            client = new TelegramBotClient(token);
            Parser = parser;
            Logger = logger;
            serializer = new TelegramBotSerializer();
        }

        public void StartReceiving()
        {
            cts = new CancellationTokenSource();

            Users = serializer.ReadUsers();

            client.StartReceiving(new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync), cts.Token);
            
            Logger.Log($"Started Working");
            timer = new MailingTimer(client, Users, Parser);
            Console.ReadKey();

            cts.Cancel();
            serializer.SaveUsers(Users);
        }

        private Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            var result = Task.CompletedTask;
            switch (update.Type)
            {
                case UpdateType.Message:
                    if (update.Message.Type != MessageType.Text)
                        return SendErrorSticker(update.Message.Chat.Id);
                    result = MessageHandler(update);
                    break;
                case UpdateType.CallbackQuery:
                    result = OnCallbackQuery(update);
                    break;
                default:
                    result = SendErrorSticker(update.Message.Chat.Id);
                    break;
            }

            MailingTimer.Users = Users;

            return result;
        }

        private Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var errorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };
            Logger.LogException(exception);
            Console.WriteLine(errorMessage);
            int cnt = 1;
            if (errorMessage.Contains("Too Many "))
            {
                var split = errorMessage.Split(" ");
                cnt = int.Parse(split[split.Length - 1]);
            }
            
            Thread.Sleep(cnt * 1000);
            if(client.IsReceiving)
                client.StopReceiving();
            client.StartReceiving(new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync), cts.Token);
            return Task.CompletedTask;
        }

        private User GetAndCheckUser(long key)
        {
            if (!Users.ContainsKey(key))
                return null;
            var user = Users[key];
            foreach (var each in user.GetType().GetFields())
                if (each.GetValue(user) == null)
                {
                    client.SendTextMessageAsync(key,
                    "Вы не завершили регистрацию!");
                    return null;
                }
            return user;
        }

        private Task TryIfText(Message msg)
        {
            Task result = null;
            switch (msg.Text)
            {
                case "/start":
                    result = client.SendTextMessageAsync(msg.Chat.Id,
                        "Привет! Жду указаний!",
                        replyMarkup: MenuButtons());
                    break;
                case "/unregister":

                    Users.Remove(msg.From.Id);
                    result = client.SendTextMessageAsync(msg.Chat.Id,
                        "Регистрация успешно удалена.",
                        replyMarkup: new ReplyKeyboardRemove());
                    serializer.SaveUsers(Users);
                    break;
                case "/switchMailing":
                    Users[msg.From.Id].NeedMailing = !Users[msg.From.Id].NeedMailing;
                    serializer.SaveUsers(Users);
                    var text = Users[msg.From.Id].NeedMailing ? "Рассылка включена" : "Рассылка выключена";
                    result = client.SendTextMessageAsync(msg.Chat.Id,
                        text,
                        replyMarkup: MenuButtons());
                    break;

            }
            return result;
        }

        private Task MessageHandler(Update update)
        {
            //return Task.CompletedTask;
            var msg = update.Message;
            var text = "";
            var flag = true;
            var user = GetAndCheckUser(msg.Chat.Id);
            if (user == null)
                return Registration(update);

            var result = TryIfText(msg);
            if (result != null)
                return result;
            else
                result = Task.CompletedTask;
            List<Lesson> lessons;
            var index = Array.IndexOf(buttonNames, msg.Text);
            switch ((Buttons)index)
            {
                case Buttons.TodayLessons:
                    lessons = Parser.GetTodayLessons(Users[msg.Chat.Id]);
                    text = $"Расписание на {DateTime.Now.ToShortDateString()}\n{LessonsToString(lessons)}";
                    break;
                case Buttons.TomorrowLessons:
                    lessons = Parser.GetTomorrowLessons(Users[msg.Chat.Id]);
                    text = $"Расписание на {DateTime.Now.AddDays(1).ToShortDateString()}\n{LessonsToString(lessons)}";
                    break;
                case Buttons.LessonNow:
                    lessons = Parser.GetLessonNow(Users[msg.Chat.Id]);
                    if(lessons.Count==0 || lessons[0].Number==-1)
                        text= "Сейчас пары нет";
                    else
                        text = $"{LessonsToString(lessons)}";
                    break;
                case Buttons.LessonNext:
                    lessons = Parser.GetNextLesson(Users[msg.Chat.Id]);
                    if(lessons.Count==0 || lessons[0].Number==-1)
                        text= "Следующей пары нет";
                    else
                        text = $"{LessonsToString(lessons)}";
                    break;
                case Buttons.TransportSG:
                    text = new TransportSchedule().GetTransportSchedule(url_sg);
                    break;
                case Buttons.TransportUG:
                    text = new TransportSchedule().GetTransportSchedule(url_ug);
                    break;
                default:
                    flag = false;
                    result = SendErrorSticker(msg.Chat.Id);
                    break;
            }

            if (flag)
                result = client.SendTextMessageAsync(msg.Chat.Id,
                    text,
                    replyMarkup: MenuButtons());
            return result;
        }

        private Task OnCallbackQuery(Update update)
        {
            var result = Task.CompletedTask;
            var clbk = update.CallbackQuery;
            if (!Users.ContainsKey(clbk.From.Id))
                Users.Add(clbk.From.Id, new User(clbk.From.Id));

            var user = Users[clbk.From.Id];
            switch (clbk.Data)
            {
                case "SubGroup: 0":
                    {
                        user.SubGroup = SubGroups.Первая;
                        result = client.AnswerCallbackQueryAsync(clbk.Id, $"Выбрано\nПодгруппа - {user.SubGroup}", true);
                        break;
                    }
                case "SubGroup: 1":
                    {
                        user.SubGroup = SubGroups.Вторая;
                        result = client.AnswerCallbackQueryAsync(clbk.Id, $"Выбрано\nПодгруппа - {user.SubGroup}", true);
                        break;
                    }
                //case "course: ТФКП":
                //    {
                //        user.Course = Courses.ТФКП;
                //        result = client.AnswerCallbackQueryAsync(clbk.Id, $"Выбрано\nКурс по выбору - {user.Course}", true);
                //        break;
                //    }
                //case "course: Топология":
                //    {
                //        user.Course = Courses.Топология;
                //        result = client.AnswerCallbackQueryAsync(clbk.Id, $"Выбрано\nКурс по выбору - {user.Course}", true);
                //        break;
                //    }
                case "done":
                    {
                        if (user != null/* && user.Course != null*/ && user.SubGroup != null)
                        {
                            client.DeleteMessageAsync(clbk.Message.Chat.Id, clbk.Message.MessageId);
                            client.AnswerCallbackQueryAsync(clbk.Id, $"Подгруппа: {user.SubGroup}"/*\nКурс по выбору: {user.Course}"*/, true);
                            result = client.SendTextMessageAsync(clbk.Message.Chat.Id, "Отлично!\nТеперь напиши или нажми => /start");
                        }
                        else
                            result = client.AnswerCallbackQueryAsync(clbk.Id, $"Выбраны не все подгруппы.\nВыберите и нажмите ещё раз", true);
                        break;
                    }
            }

            serializer.SaveUsers(Users);

            return result;
        }

        private Task Registration(Telegram.Bot.Types.Update update)
        {
            var result = Task.CompletedTask;
            var msg = update.Message;

            var inlineKeyboard = new InlineKeyboardMarkup(new[]
            {
                new []
                {
                    InlineKeyboardButton.WithCallbackData(text: "Первая подгруппа", callbackData: "SubGroup: 0"),
                    InlineKeyboardButton.WithCallbackData(text: "Вторая подгруппа", callbackData: "SubGroup: 1"),
                },
                //new []
                //{
                //    InlineKeyboardButton.WithCallbackData(text: "ТФКП", callbackData: "course: ТФКП"),
                //    InlineKeyboardButton.WithCallbackData(text: "Топология", callbackData: "course: Топология"),
                //},
                new []
                {
                    InlineKeyboardButton.WithCallbackData(text: "Зарегистрироваться", callbackData: "done"),
                }
            });

            if (!Users.ContainsKey(msg.From.Id))
                Users.Add(msg.From.Id, new User(msg.From.Id));
            result = client.SendTextMessageAsync(msg.Chat.Id, "Для продолжения работы нужно пройти регистрацию!\nВыберите подгруппы и нажмите кнопку \"Зарегистрироваться\"", replyMarkup: inlineKeyboard);
            serializer.SaveUsers(Users);
            return result;
        }

        private Task SendErrorSticker(long id)
        {
            var file = new Telegram.Bot.Types.InputFiles.InputOnlineFile(
                "CAACAgIAAxkBAAIBmWFf8Ia0tHtyLUI9Pg2cfe2Pz87tAAIuAwACtXHaBqoozbmcyVK2IQQ");
            return client.SendStickerAsync(id, file);
        }

        public static IReplyMarkup MenuButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>
                {
                    new ()
                    {
                        new KeyboardButton { Text = buttonNames[(int)Buttons.TodayLessons]},
                        new KeyboardButton { Text = buttonNames[(int)Buttons.TomorrowLessons]}
                    },
                    new ()
                    {
                        new KeyboardButton { Text = buttonNames[(int)Buttons.LessonNow]},
                        new KeyboardButton { Text = buttonNames[(int)Buttons.LessonNext]}
                    },
                    new ()
                    {
                        new KeyboardButton { Text = buttonNames[(int)Buttons.TransportSG]},
                        new KeyboardButton { Text = buttonNames[(int)Buttons.TransportUG]}
                    }
                }
            };
        }

        public static string LessonsToString(List<Lesson> lessons)
        {
            var result = new StringBuilder();
            foreach (var lesson in lessons)
            {
                result.AppendLine(string.Format($"{lesson.Number}: {lesson.Name} Кабинет: {lesson.ClassRoom}"));
            }

            return result.ToString();
        }
    }
}
