﻿namespace Google_Docs
{
    class MainClass
    {
        private static string token = "2034947215:AAEa5Efp5wpFyGCZPnkUi2IaQpz5k85dXQQ";
        static void Main()
        {
            var logger = new FileLogger("log.txt");

            var parser = new TableToDaysParser(logger);

            var telegramBot = new TelegramBot(token, parser, logger);

            telegramBot.StartReceiving();
        }
    }
}
