using System;
using System.IO;
public class FileLogger : IMyLogger
{
    private string fileName;
    public FileLogger(string fileName)
    {
        this.fileName = fileName;
    }
    public void Log(string data)
    {
        using (var writer = new StreamWriter(fileName))
        {
            writer.WriteLine($"[{DateTimeOffset.Now}]");
            writer.WriteLine(data);
        }
    }

    public void LogException(Exception data)
    {
        using (var writer = new StreamWriter(fileName))
        {
            writer.WriteLine($"[{DateTimeOffset.Now}]");
            writer.WriteLine("[ERROR]");
            writer.WriteLine(data.Message);
            writer.WriteLine(data.Source);
            writer.WriteLine(data.StackTrace);
        }
    }
}