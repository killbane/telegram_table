﻿namespace Google_Docs
{
    public enum Courses
    {
        ТФКП=0,
        Топология= 1
    }
    public enum SubGroups
    {
        Первая=1,
        Вторая=2
    }
    public class User
    {
        public long Id;
        public SubGroups? SubGroup; 
        //public Courses? Course;
        public bool NeedMailing;

        public User(long id)
        {
            Id = id;
        }
    }
}
