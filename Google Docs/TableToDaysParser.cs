﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Google_Docs
{
    public class TableToDaysParser : ITableWorker
    {
        IMyLogger logger;
        public TableToDaysParser(IMyLogger logger)
        {
            this.logger = logger;
        }
        private static List<Lesson> ClearResult(List<Lesson> result)
        {
            for (int i = 0; i < result.Count; i++)
                result[i].Number = i + 1;
            for (var i = 0; i < result.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(result[i].Name) || result[i].Name.Contains("--") || result[i].Name == "окно")
                    result.Remove(result[i]);
                else
                    break;
            }
            for (var i = result.Count-1; i >= 0; i--)
                if (string.IsNullOrWhiteSpace(result[i].Name) || result[i].Name.Contains("--") || result[i].Name == "окно")
                    result.RemoveAt(i);
                else
                    break;

            return result;
        }

        private List<Lesson> GetLessonsFromTable(IList<IList<object>> tableValues, int todayWeekDayNumber, int weekSwitcher)
        {
            var result = new List<Lesson>();

            if (todayWeekDayNumber < 0)
            {
                result.Add(new Lesson() { ClassRoom = "Дом", Name = "Выходной" });
                return result;
            }

            for (var i = 0; i < 5; i++)
                if (tableValues[i].Count > todayWeekDayNumber)
                    result.Add(new Lesson()
                    {
                        Name = tableValues[i + 5 * weekSwitcher][todayWeekDayNumber].ToString().Replace('\n', ' '),
                        ClassRoom = tableValues[i + 5 * weekSwitcher][todayWeekDayNumber + 1].ToString()
                    });
                else
                    result.Add(new Lesson()
                    {
                        Name = "окно",
                        ClassRoom = "нигде"
                    });

            return result;
        }

        private List<Lesson> GetLessonsFromTable(IList<IList<object>> tableValues, int todayWeekDayNumber, int weekSwitcher, User user)
        {
            var result = new List<Lesson>();
            var type = user.GetType();
            foreach (var each in type.GetFields())
                if (each.GetValue(user) == null)
                    return GetLessonsFromTable(tableValues, todayWeekDayNumber, weekSwitcher);
            if (todayWeekDayNumber < 0)
            {
                result.Add(new Lesson { ClassRoom = "Дом", Name = "Выходной" });
                return result;
            }
            for (var i = 0; i < 5; i++)
            {
                Lesson newLesson;
                if (tableValues[i].Count > todayWeekDayNumber)
                {
                    var name = tableValues[i + 5 * weekSwitcher][todayWeekDayNumber].ToString().Replace('\n', ' ');
                    var classRoom = tableValues[i + 5 * weekSwitcher][todayWeekDayNumber + 1].ToString();

                    if (name.Contains("|"))
                    {
                        if (name.Contains("(II)") || name.Contains("(I)"))
                        {
                            var nameSplit = name.Split('|');
                            if (name.Contains($"({new string('I', (int)user.SubGroup)})"))
                            {
                                name = nameSplit.First(predicate: x => x.Contains($"({new string('I', (int)user.SubGroup)})"));
                            }
                            else
                            {
                                name = nameSplit.First(predicate: x => (!x.Contains($"({new string('I', 3 - (int)user.SubGroup)})") && !x.Contains($"({new string('I', (int)user.SubGroup)})")));
                            }
                            if (classRoom.Contains('|'))
                                classRoom = classRoom.Split('|')[Array.IndexOf(nameSplit, name)];
                        }
                        //else
                        //{
                        //    name = name.Split('|')[(int)user.Course];
                        //    if (classRoom.Contains('|'))
                        //        classRoom = classRoom.Split('|')[(int)user.Course];
                        //}
                    }
                    name.Trim();
                    newLesson = new Lesson
                    {
                        Name = name,
                        ClassRoom = classRoom
                    };
                }
                else
                    newLesson = new Lesson
                    {
                        Name = "окно",
                        ClassRoom = "нигде"
                    };
                result.Add(newLesson);
            }
            return result;
        }

        public List<Lesson> GetTodayLessons(User user)
        {
            var worker = new TableWorker(logger);
            var tableValues = worker.GetTableData();

            var todayWeekDayNumber = (int)(DateTime.Today.DayOfWeek - 1) * 3;

            var weekSwitcher = 0;
            var weekCounter = DateTime.Today;
            if (weekCounter.Month >= 9)
            {
                weekCounter = new DateTime(weekCounter.Year, 9, 1);
                weekCounter = weekCounter.AddDays(DateTime.Today.DayOfWeek - weekCounter.DayOfWeek);

                var weeksAmount = DateTime.Today - weekCounter;
                weekSwitcher = (weeksAmount.Days / 7) % 2;
            }
            else
            {
                weekCounter = new DateTime(weekCounter.Year, 1, 1);
                weekCounter = weekCounter.AddDays(DateTime.Today.DayOfWeek - weekCounter.DayOfWeek);

                var weeksAmount = DateTime.Today - weekCounter;
                weekSwitcher = (weeksAmount.Days / 7 + 1) % 2;
            }

            var result = GetLessonsFromTable(tableValues, todayWeekDayNumber, weekSwitcher, user);

            return ClearResult(result);
        }

        public List<Lesson> GetTomorrowLessons(User user)
        {
            var worker = new TableWorker(logger);
            var tableValues = worker.GetTableData();

            var todayWeekDayNumber = (int)(DateTime.Today.AddDays(1).DayOfWeek - 1) * 3;

            var weekSwitcher = 0;
            var weekCounter = DateTime.Today.AddDays(1);
            if (weekCounter.Month >= 9)
            {
                weekCounter = new DateTime(weekCounter.Year, 9, 1);
                weekCounter = weekCounter.AddDays(DateTime.Today.DayOfWeek - weekCounter.DayOfWeek);

                var weeksAmount = DateTime.Today.AddDays(1) - weekCounter;
                weekSwitcher = (weeksAmount.Days / 7 ) % 2;
            }
            else
            {
                weekCounter = new DateTime(weekCounter.Year, 1, 1);
                weekCounter = weekCounter.AddDays(DateTime.Today.DayOfWeek - weekCounter.DayOfWeek);

                var weeksAmount = DateTime.Today.AddDays(1) - weekCounter;
                weekSwitcher = (weeksAmount.Days / 7+1) % 2;
            }

            var result = GetLessonsFromTable(tableValues, todayWeekDayNumber, weekSwitcher, user);

            return ClearResult(result);
        }

        public List<Lesson> GetLessonNow(User user)
        {
            var todayLessons = GetTodayLessons(user);

            var timeNow = DateTime.Now;
            var timeWithoutDate = new DateTime(1, 1, 1, timeNow.Hour, timeNow.Minute, timeNow.Second);
            var timeOfLessonStart = new DateTime(1, 1, 1, 8, 50, 0);
            var timeOfLessonEnd = new DateTime(1, 1, 1, 10, 35, 0);
            int lessonNuber = 1;
            for (var i = 0; i < todayLessons.Count; i++)
            {
                if (timeOfLessonStart <= timeWithoutDate && timeWithoutDate <= timeOfLessonEnd)
                {
                    if (todayLessons.Count > i && !string.IsNullOrEmpty(todayLessons[i].Name) && todayLessons[i].Number==lessonNuber)
                        return new List<Lesson>(){todayLessons[i]};
                }
                timeOfLessonStart = timeOfLessonEnd;
                timeOfLessonEnd = timeOfLessonEnd.AddHours(1);
                timeOfLessonEnd = timeOfLessonEnd.AddMinutes(45);
                if (timeOfLessonStart.Hour == 12)
                    timeOfLessonEnd = timeOfLessonEnd.AddMinutes(50);
                if(todayLessons[i].Number!=lessonNuber)
                    i--;
                lessonNuber++;
            }

            return new List<Lesson>();
        }

        public List<Lesson> GetNextLesson(User user)
        {
            var todayLessons = GetTodayLessons(user);
            var timeNow = DateTime.Now;
            var timeWithoutDate = new DateTime(1, 1, 1, timeNow.Hour, timeNow.Minute, timeNow.Second);
            var timeOfLessonStart = new DateTime(1, 1, 1, 8, 50, 0);
            var timeOfLessonEnd = new DateTime(1, 1, 1, 10, 35, 0);

            int lessonNuber = 1;
            for (var i = 0; i < todayLessons.Count; i++)
            {
                if (timeOfLessonStart <= timeWithoutDate && timeWithoutDate <= timeOfLessonEnd)
                {
                    if (todayLessons.Count > i + 1 && !string.IsNullOrEmpty(todayLessons[i + 1].Name) && todayLessons[i].Number==lessonNuber)
                        return new List<Lesson>(){todayLessons[i + 1]};
                    if (todayLessons.Count > i && !string.IsNullOrEmpty(todayLessons[i].Name) && todayLessons[i].Number==lessonNuber+1)
                        return new List<Lesson>(){todayLessons[i]};
                }
                timeOfLessonStart = timeOfLessonEnd;
                timeOfLessonEnd = timeOfLessonEnd.AddHours(1);
                timeOfLessonEnd = timeOfLessonEnd.AddMinutes(45);
                if (timeOfLessonStart.Hour == 12)
                    timeOfLessonEnd = timeOfLessonEnd.AddMinutes(50);
                if(todayLessons[i].Number!=lessonNuber)
                    i--;
                lessonNuber++;
            }

            return new List<Lesson>();
        }
    }
}
