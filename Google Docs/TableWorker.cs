﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

namespace Google_Docs
{
    class TableWorker
    {
        IMyLogger logger;
        private static DateTimeOffset previousDownload;
        private static IList<IList<object>> previousData;
        public TableWorker(IMyLogger logger){
            this.logger = logger;
        } 
        static readonly string[] Scopes =
        {
            SheetsService.Scope.SpreadsheetsReadonly
        };

        static string ApplicationName =
            "TelegramTable";

        public IList<IList<object>> GetTableData()
        {
            if(previousData!=null)
            {
                var distance = DateTimeOffset.Now - previousDownload;
                if (distance.TotalMinutes < 1)
                    return previousData;
            }

            UserCredential credential;
            var path = "credentials.json";
            using (var stream =
                new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                const string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("getting data from table");
                Console.WriteLine(DateTimeOffset.Now);
            }
            
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            
            var spreadsheetId = "1xmCF4lOoXVrOWLVyCCAlpBs70lescTFBk1yVAqL-A_w";
            var range = "C2:S11";
            var request = service.Spreadsheets.Values.Get(spreadsheetId, range);
            try
            {
                var response = request.Execute();
                previousData = response.Values;
                previousDownload = DateTimeOffset.Now;
                return response.Values;
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                Console.WriteLine(ex.Message);
                Thread.Sleep(1000);
                return GetTableData();
            }
        }
    }
}
