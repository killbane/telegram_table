﻿using System.Collections.Generic;

namespace Google_Docs
{
    public interface ITableWorker
    {
        List<Lesson> GetTodayLessons(User user);
        List<Lesson> GetTomorrowLessons(User user);
        List<Lesson> GetLessonNow(User user);
        List<Lesson> GetNextLesson(User user);
    }
}
