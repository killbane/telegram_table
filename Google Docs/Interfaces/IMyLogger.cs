using System;
public interface IMyLogger
{
    void Log(string data);
    void LogException(Exception data);
}